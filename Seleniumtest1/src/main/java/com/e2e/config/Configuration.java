package com.e2e.config;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "system:properties",
        "classpath:configuration.properties"})
public interface Configuration  extends Config{

    @Key("browser")
    String browser();
    @Key("url.base")
    String baseUrl();

    @Key("waitTime")
    int waitTime();

    @Key("pollingTime")
    int pollingTime();

    @Key("path.screenShots")
    String path2Screenshots();

    @Key("path.testData")
    String path2testData();

    @Key("ctrl.highLight")
    boolean ctrlHighLight();

    @Key("ctrl.hlTime")
    int ctrlHighLightTime();

    @Key("showReport")
    boolean showReport();

    @Key("dockingAllowed")
    boolean dockingAllowed();

    @Key("zoom.75")
    String zoom75();

    @Key("zoom.100")
    String zoom100();

    @Key("allowAllWebSocketOrigins")
    String allowAllWebSocketOrigins();

    @Key("dockSide")
    String dockSide();
}

