package com.e2e.pages.authentication;

import com.e2e.config.ConfigurationManager;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;

import static com.e2e.utilities.CommonUtil.buildWebElement;
import static com.e2e.utilities.FrontActionsUtil.actionScroll2Element;

public class BasePage {

    public static final int TIMEOUT = ConfigurationManager.configuration().waitTime();
    public static final int POLLING = ConfigurationManager.configuration().pollingTime();
    private WebDriver driver;
    Select select;
    protected String CONTAINS_TEXT_XPATH = "//*[contains(text(),\"%s\")]";
    protected final WebDriverWait wait;
    private By successMessage = By.xpath("//*[contains(text(),'Jordan Klechov')]");

    public BasePage(WebDriver driver) {
                this.driver = driver;
                wait = new WebDriverWait(driver, Duration.ofSeconds(TIMEOUT), Duration.ofMillis(POLLING));
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, TIMEOUT), this);
    }

    /**
     * Refresh the page
     */
    public void refreshPage(){driver.navigate().refresh();}

    /**
     * Select option from dropdown menu by visible text
     *
     * @param element to be selected from
     * @param option to be selected
     */
    protected void selectOptionFromDropDownMenu(WebElement element, String option ){
      select = new Select(element);
      wait.until(ExpectedConditions.elementToBeClickable(element));
      select.selectByVisibleText(option);
}
    /**
     * Returns you to the previous page
     */
    public void returnBack(){driver.navigate().back();}

    /**
     * Wait until element is clickable and click it
     *
     * @param element        has to be waited and clicked
     * @return
     */
    protected void waitAndClick(WebElement element){
        wait.until(ExpectedConditions.elementToBeClickable(element));
        actionScroll2Element(driver, element);
        element.click();
    }

    /**
     * Wait until element is clickable and click it with JS executor
     *
     * @param element        has to be waited and clicked
     * @return
     */
    //protected void waitAndJSClick(WebElement element){
      //  wait.until(ExpectedConditions.elementToBeClickable(element));
        //actionScroll2Element(driver, element);
       // clkElementByJS(driver, element);
    //}
    /**
     * Wait until element is clickable, scroll to it, and send keys
     *
     * @param element has to be waited and clicked
     * @param string that has to bi entered
     */
    public void waitAndSendKeys(WebElement element, String string){
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        element.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        element.sendKeys(Keys.DELETE);
        element.sendKeys(string);
    }

    /**
     * Tries to build element and return if the element is displayed ot not.
     *
     * @param xpath the Xpath of the element that has to be build
     * @param text addition to the Xpath. The text that elelement contains.
     * @return true if element is displayed, if not - false
     */
    public boolean isElementPresentByXpath(String xpath, Object... text){
        try {
            WebElement element = buildWebElement(driver, wait, xpath, text);
            return element.isDisplayed();
        } catch (TimeoutException e){
            return false;
    }
}

    /**
     * Attempt to find element and return that it is displayed, if not, return false
     *
     * @return true if element is displayed, if not - false
     */
    public boolean isElementPresent(WebElement element){
   try {
       wait.until(ExpectedConditions.elementToBeClickable(element));
       return element.isDisplayed();
   } catch (TimeoutException | NoSuchElementException e){
       return false;
   }
}

    /**
     * Switches between tabs and Selenium can know where it is right now
     *
     * @param tabIndex to be switched to (0 is the first tab, 1 - second, 2 - third....)
     */
    public void switchTab(int tabIndex){
    ArrayList<String> tabs2 = new ArrayList<> (driver.getWindowHandles());
            driver.switchTo().window(tabs2.get(tabIndex));
}

    /**
     * Assert if success message is displayed
     *
     * @param message to be asserted
     * @return true if displayed and false if not
     */
    public boolean isSuccessMessageDisplayed(String message){
return isElementPresentByXpath(CONTAINS_TEXT_XPATH, message);
    }


    /**
     * Assert if error message is displayed
     *
     * @param message to be asserted
     * @return true if displayed, false if not
     */
    public boolean isErrorMessageDisplayed(String message){
        if (!message.isEmpty()) {
            return isElementPresentByXpath(CONTAINS_TEXT_XPATH, message);
        }
        return true;
    }

    /**
     *Upload a file to the selected input element
     * @param file string path to be uploaded
     */
    public void uploadFile(WebElement element, String file){element.sendKeys(file);}

    /**
     *
     * @return
     */
public <TPage extends BasePage> TPage waitAndClickElementAndReturnNewPage(Class<TPage> pageClass, WebElement element){
    actionScroll2Element(driver, element);
    waitAndClick(element);
    try {
        return pageClass.getDeclaredConstructor(WebDriver.class).newInstance(driver);
    } catch (Exception e) {
        e.printStackTrace();
        return null;
    }
}
    public String getSuccessMessage() {
                return driver.findElement(successMessage).getText();
    }
}