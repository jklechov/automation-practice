package com.e2e.pages.authentication;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignUpPage extends BasePage{


    @FindBy(id = "password")
    private WebElement loc_inputPassword;
    @FindBy(id = "first_name")
    private WebElement loc_inputFirstName;

    @FindBy(id = "last_name")
    private WebElement loc_inputLastName;

    @FindBy(id = "address1")
    private WebElement loc_inputAddress;

    @FindBy(id = "country")
    private WebElement loc_selectCountry;

    @FindBy(id = "state")
    private WebElement loc_inputState;

    @FindBy(id = "city")
    private WebElement loc_inputCity;

    @FindBy(id = "zipcode")
    private WebElement loc_inputZipCode;

    @FindBy(id = "mobile_number")
    private WebElement loc_inputMobileNumber;

    @FindBy(xpath = "//button[@data-qa='create-account']")
    private WebElement loc_btnCreateAccount;

    public void setPassword(String password){
        waitAndSendKeys(loc_inputPassword, password);
    }

    public void setFirstName(String firstName){
        waitAndSendKeys(loc_inputFirstName, firstName);
    }

    public void setLastName(String lastName){
        waitAndSendKeys(loc_inputLastName, lastName);
    }

    public void setAddress(String address){
        waitAndSendKeys(loc_inputAddress, address);
    }

    public void selectCountry(String country){
       selectOptionFromDropDownMenu(loc_selectCountry, country);
    }

    public void setState(String state){
        waitAndSendKeys(loc_inputState, state);
    }

    public void setCity(String city){
        waitAndSendKeys(loc_inputCity, city);
    }

    public void setZipCode(String zipCode){
        waitAndSendKeys(loc_inputZipCode, zipCode);
    }

    public void setMobileNumber(String mobileNumber){
        waitAndSendKeys(loc_inputMobileNumber, mobileNumber);
    }
    public AccountCreatedPage clkCreateAccountBtn(){
        return waitAndClickElementAndReturnNewPage(AccountCreatedPage.class, loc_btnCreateAccount);
    }

    public AccountCreatedPage setFormAndCompleteSignUp(String password, String firstName, String lastName,
                             String address, String country, String state, String city,String
                                                           zipCode, String mobileNumber){
        setPassword(password);
        setFirstName(firstName);
        setLastName(lastName);
        setAddress(address);
        selectCountry(country);
        setState(state);
        setCity(city);
        setZipCode(zipCode);
        setMobileNumber(mobileNumber);
return clkCreateAccountBtn();
    }


    public SignUpPage(WebDriver driver) {
        super(driver);
    }
}
