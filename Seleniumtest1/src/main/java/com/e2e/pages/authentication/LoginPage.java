package com.e2e.pages.authentication;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {
    private WebDriver driver;
    private By emailInput = By.name("email");
    private By nameInput = By.name("name");
    private By passwordInput = By.name("password");
    private By loginButton = By.xpath("//button[contains(text(),'Login')]");
    @FindBy(xpath = "//input[@data-qa='signup-name']")
    private WebElement loc_inputName;
    @FindBy(xpath = "//input[@data-qa='signup-email']")
    private WebElement loc_inputEmail;
    @FindBy(xpath = "//button[@data-qa='signup-button']")
    private WebElement loc_btnSignup;


    /**
     * Sets the name on the Login page
     *
     * @param name to be set
     */
    public void setName(String name) {
        waitAndSendKeys(loc_inputName, name);
    }

    /**
     * Sets the name on the Login page
     *
     * @param email to be set
     */
    public void setEmail(String email) {
        waitAndSendKeys(loc_inputEmail, email);
    }

    /**
     * Clicks the "SignUp" button on the Login Page
     * @return new instance of SignUp page
     */
    public SignUpPage clkSignUpBtn(){
        return waitAndClickElementAndReturnNewPage(SignUpPage.class, loc_btnSignup);
    }

    /**
     * Sets form and clicks on "SignUp" button
     * @param name to be set
     * @param email to be set
     * @return new instance of SignUp page
     */
    public SignUpPage setFormAndSignUp(String name, String email){
        setName(name);
        setEmail(email);
        return clkSignUpBtn();
    }
    public LoginPage(WebDriver driver) {
        super(driver);
    }

        //public void enterEmail(String email) {
        //     driver.findElement(emailInput).sendKeys(email);
        // }
        // public void enterPassword(String password) {
        //     driver.findElement(passwordInput).sendKeys(password);
        // }
        // public void clickLoginButton() {
        //    driver.findElement(loginButton).click();
        //  }
        // public void enterName(String name){
        // driver.findElement(nameInput).sendKeys(name);
        //  }
    }
