package com.e2e.pages.authentication;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountCreatedPage extends BasePage{

    @FindBy(xpath = "//button[@data-qa='continue-button']")
    private WebElement loc_btnContinue;

    public boolean isContinueBtnPresent(){
        return isElementPresent(loc_btnContinue);
    }
    public AccountCreatedPage(WebDriver driver) {
        super(driver);
    }
}
