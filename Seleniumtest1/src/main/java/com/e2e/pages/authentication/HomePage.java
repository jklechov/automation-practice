package com.e2e.pages.authentication;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{

    @FindBy(xpath = "//a[contains(text(),' Signup / Login')]")
    private WebElement locSignUpLogInBtn;

    /**
     * Clicks Login/SignUp button on home page
     * @return new instance of Login page
     */
    public LoginPage clkSignUpLogInBtn() {
       return waitAndClickElementAndReturnNewPage(LoginPage.class, locSignUpLogInBtn);
    }


    public HomePage(WebDriver driver) {
        super(driver);
    }
}
