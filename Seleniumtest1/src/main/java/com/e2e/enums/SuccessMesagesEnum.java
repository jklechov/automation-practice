package com.e2e.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor

public enum SuccessMesagesEnum {
    ACCOUNT_CREATED_SUCCESSFULLY("Congratulations! Your new account has been successfully created!");
    public final String message;

    SuccessMesagesEnum(String message) {
        this.message = message;
    }
}
