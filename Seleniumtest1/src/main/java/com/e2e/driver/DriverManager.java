package com.e2e.driver;

import com.e2e.config.ConfigurationManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

public class DriverManager {

    static String zoom75percent = ConfigurationManager.configuration().zoom75();
    static String zoom100percent = ConfigurationManager.configuration().zoom100();
    private static final String CHROME_FIX = ConfigurationManager.configuration().allowAllWebSocketOrigins();

    public static WebDriver createInstance(String browserName){
        WebDriver driver = null;
        DriverType browser= DriverType.getDriverByName(browserName);

        switch (browser){
            case CHROME:
                ChromeOptions options = new ChromeOptions();
                WebDriverManager.chromedriver().setup();
                options.addArguments(zoom100percent, CHROME_FIX);
                driver = new ChromeDriver();
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            case EDGE:
                /* todo */
                break;
        }

        return driver;
    }

    public static void dockBrowserWindow(String dockSide){
        try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_WINDOWS);
            if(dockSide=="right"){ robot.keyPress(KeyEvent.VK_RIGHT); } else { robot.keyPress(KeyEvent.VK_LEFT); }
            robot.keyRelease(KeyEvent.VK_WINDOWS);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

}

