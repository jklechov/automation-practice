package com.e2e.dataproviders;

import org.testng.annotations.DataProvider;

public class SignUpDataProvider extends BaseDataProvider{

    @DataProvider(name = "invalidSignUp")
    public static Object[][] invalidSignUp() {
        return new Object[][]{
                {}
        };
    }
    @DataProvider(name = "validSignUp")
    public static Object[][] validSignUp() {
        return new Object[][]{
                {randomFirstName, randomEmail, "parolka123", randomFirstName, randomLastName,
                "address", "Canada", "state", "city", "123456", "0885666777"}
        };
    }
}
