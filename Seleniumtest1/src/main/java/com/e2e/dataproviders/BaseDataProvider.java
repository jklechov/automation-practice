package com.e2e.dataproviders;
import com.github.javafaker.Faker;
public class BaseDataProvider {

    //File path for a .JPG file in the folder when needed
    static String directoryPath = System.getProperty("user.dir");
    static String separator = System.getProperty("file.separator");
    protected static String  filePath = directoryPath + separator + ".jpg";

    static Faker faker = new Faker();
    protected static String randomEmail = faker.internet().emailAddress();
    protected static String randomFirstName = faker.name().name();
    protected static String randomLastName = faker.name().name();
    //protected static String password = faker.random().

}
