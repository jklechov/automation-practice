package com.e2e;


import com.e2e.dataproviders.SignUpDataProvider;
import com.e2e.enums.SuccessMesagesEnum;
import com.e2e.pages.authentication.AccountCreatedPage;
import com.e2e.pages.authentication.HomePage;
import com.e2e.pages.authentication.LoginPage;
import com.e2e.pages.authentication.SignUpPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SignUpTest extends TestBase{
    HomePage homePage;
    LoginPage loginPage;
    SignUpPage signUpPage;
    AccountCreatedPage accountCreatedPage;
    @Override
    protected void preconditions() {

    }
    @Test(enabled = true,
           priority = 1,
           dataProviderClass = SignUpDataProvider.class,
            dataProvider = "invalidSignUp",
            description = "description")
    public void validSignUp(String name, String email, String password, String firstName, String lastName,
                            String address, String country, String state, String city,String
                                        zipCode, String mobileNumber){
        homePage = new HomePage(getDriver());
        loginPage = homePage.clkSignUpLogInBtn();
        signUpPage = loginPage.setFormAndSignUp(name, email);
        accountCreatedPage = signUpPage.setFormAndCompleteSignUp(password, firstName, lastName,
                address, country, state, city, zipCode, mobileNumber);
        Assert.assertTrue(accountCreatedPage.isContinueBtnPresent(),
                "Continue button is not present!!!");
        Assert.assertTrue(accountCreatedPage.isSuccessMessageDisplayed(SuccessMesagesEnum.ACCOUNT_CREATED_SUCCESSFULLY.message));
    }
}
